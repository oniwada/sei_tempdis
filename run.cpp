#define _Niter_Multiply_
#include"_SEI_Dis_.h"
#include<random>
//#define DATABASE ":memory:"
int main(int Nargs, char*Inputs[]){

 int rank, size;
 MPI_Init(&Nargs, &Inputs);
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);
 uint64_t NMeans = 20000;
 if(rank == 0){
	std::random_device rnd;
	_MPI_vector_<long long> seed(1);
	for(int process = 1; process < size; process++){
		seed[0] = rnd();
		seed.Send(process, 0);
	}
	_SEI_Dis_ Simul(time(NULL),Inputs[1],Inputs[2]);
	Simul.Save.Exec("PRAGMA page_size=262144;");
	Simul.Save.Exec("PRAGMA cache_size=1000000;");
	Simul.Save.Exec("PRAGMA temp_store=2;");
	Simul.Save.Exec("PRAGMA synchronous=1;");
	Simul.Save.Exec("PRAGMA mmap_size=2097152;");
	Simul.Save.Exec("PRAGMA threads=4;");
	Simul.Save.Exec("VACUUM;");
	Simul.Save.Exec("PRAGMA threads;PRAGMA cache_size;PRAGMA synchronous;", _SQLite_Func_::print);
	_Parallelize_CondTimeSeries_Multiply_ ParSimul(+Simul, Inputs[3], rank, size);
//	_Parallelize_CondTimeSeries_ ParSimul(+Simul, Inputs[3], rank, size);
	ParSimul.Set_NTS_perSlave(NMeans);
	ParSimul.run();
 }
 else{
	_MPI_vector_<long long> seed(1);
	seed.Recv(0, 0);
	_SEI_Dis_ Simul(seed[0], NMeans);
	_Parallelize_CondTimeSeries_Multiply_ ParSimul(+Simul, rank, size);
//	_Parallelize_CondTimeSeries_ ParSimul(+Simul, rank, size);
	ParSimul.Set_NTS_perSlave(NMeans);
	ParSimul.run();
 }

 MPI_Finalize();
 return 0;
}
