#ifndef _SEI_Dis__H
#define _SEI_Dis__H

#include<vector>
#include<string>
#include<iterator>
#include<cmath>
#include<random>
#include<sstream>
#include<iomanip>

#include"_Generic_Simulation_.h"
#include"_MPI_vector_.h"
#include"_SQLite_Database_.h"

#define OTHER_DATABASEMETHOD
#define NO_MEANS
//#define STATIONARY
#define NO_CONDITION
#include"_Parallelize_CondTimeSeries_.h"
#include"_Parallelize_CondTimeSeries_Multiply_.h"
/*#include "_GaussianPowerLawRNG_.h"*/
#include "_GaussianPowerLawRNGFFTW_.h"
#ifndef _SFMT_H_
#define _SFMT_H_
#include"dSFMT.h"
#endif

struct Twouint64_t{
 double N_IS = 0;
 double N_I = 0;
 double ln_IS = 0.0;
 double ln_I = 0.0;
 double P = 0.0;

 Twouint64_t& operator+=(Twouint64_t ToAdd){
	this->N_IS += ToAdd.N_IS;
	this->N_I += ToAdd.N_I;
	this->P += ToAdd.P;
	this->ln_IS += ToAdd.ln_IS;
	this->ln_I += ToAdd.ln_I;
	return *this;
 }
 Twouint64_t& operator=(Twouint64_t ToEq){
	this->N_IS = ToEq.N_IS;
	this->N_I = ToEq.N_I;
	this->P = ToEq.P;
	this->ln_IS = ToEq.ln_IS;
	this->ln_I = ToEq.ln_I;
	return *this;
 }
 Twouint64_t& operator=(const uint64_t ToEq){
	this->N_IS = ToEq;
	this->N_I = ToEq;
	this->P = ToEq;
	this->ln_IS = ToEq;
	this->ln_I = ToEq;
	return *this;
 }
};

/*struct Twouint64_t{*/
/* double N_IS = 0;*/
/* double N_I = 0;*/

/* Twouint64_t operator+=(Twouint64_t ToAdd){*/
/*	this->N_IS += ToAdd.N_IS;*/
/*	this->N_I += ToAdd.N_I;*/
/*	return *this;*/
/* }*/
/* Twouint64_t operator=(Twouint64_t ToEq){*/
/*	this->N_IS = ToEq.N_IS;*/
/*	this->N_I = ToEq.N_I;*/
/*	return *this;*/
/* }*/
/* Twouint64_t operator=(const double ToEq){*/
/*	this->N_IS = ToEq;*/
/*	this->N_I = ToEq;*/
/*	return *this;*/
/* }*/
/*};*/

double Interpolate(double N1,double  N2,double  T1,double  T2,double  T_){
 if( fabs(T2 - T1) < 1e-10)
	return N1;
 double a = (N2-N1)/(T2 - T1);
 double b = N2 - a*T2;
 return a*T_+b;
}

struct VecWithCounter{
 std::vector<Twouint64_t> Series;
 uint64_t counter = 0;
};

class _SEI_Dis_ : public _Generic_Simulation_{
	private:
 const uint8_t NParameters = 13;
#ifndef _SAVEMOMENTUM_
 const uint8_t NResults = 2;
#else
 const uint8_t NResults = 10;
#endif // _SAVEMOMENTUM_
 double T = 0.0;
 double T_1 = 0.0;

#if _SAVEMOMENTUM_ == 2

 _MPI_vector_<double> N_IS;

#ifdef _DECAYSIMULATIONS__
 _MPI_vector_<uint64_t> T_Array;
#endif //_DECAYSIMULATIONS__

#else //_SAVEMOMENTUM_==2
 Twouint64_t State, State_1;
 uint64_t DisConf = 1;
 uint64_t DisConf_counter = 0;
 VecWithCounter TimeSeries;
#endif //_SAVEMOMENTUM_==2
 double gamma_ant = -500.;
 double LocalLambda = 0.;
 double StepSize = 0.0;
 dsfmt_t dsfmt;
#ifndef __USEFLOATFORCORRELATIONARRAY__
 fftw_real *CorrelationArray;
#else
 fftwf_real *CorrelationArray;
#endif //__USEFLOATFORCORRELATIONARRAY__

 void AssignToPointers(void);
	public:
 std::string BaseName;
 _SQLite_Database_ Save;
 _MPI_vector_<double> Parameters;
 _MPI_vector_<double> Results;
 uint64_t Histogram;
 _GaussianPowerLaw_ CorrRNGGenerator;

 _SEI_Dis_(void);
 _SEI_Dis_(uint64_t);
 _SEI_Dis_(uint64_t, uint64_t);
 _SEI_Dis_(unsigned long long , const char *, const char *);
// _SEI_Dis_(unsigned long long seed, Type Nblocks, Type Degree);

 _SEI_Dis_& Set_Parameters(void);
 bool Set_InitialConditions(void);
 _SEI_Dis_& Simulate(void);
 _SEI_Dis_& RandomSwap(void);
 _SEI_Dis_& Save_Simulation(long long);
 long long Init_Database(void);
 void BeginTransaction(void);
 void EndTransaction(void);
 uint64_t Get_Niter(void);
/* inline double GetLambda(void);*/

};


_SEI_Dis_::_SEI_Dis_(void):
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(time(NULL)){
 dsfmt_init_gen_rand(&dsfmt, time(NULL)+2);
 AssignToPointers();
}

_SEI_Dis_::_SEI_Dis_(uint64_t seed):
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(seed+1ULL){
 dsfmt_init_gen_rand(&dsfmt, seed);
 AssignToPointers();
}

_SEI_Dis_::_SEI_Dis_(uint64_t seed, uint64_t NDisConf):
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(seed+1ULL){
 DisConf = NDisConf;
 dsfmt_init_gen_rand(&dsfmt, seed);
 AssignToPointers();
}

_SEI_Dis_::_SEI_Dis_(unsigned long long seed, const char *Database, const char *Table):
	BaseName(Table),
	Save(Database, Table),
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(seed + 1ULL){
 dsfmt_init_gen_rand(&dsfmt, seed);
 Save.GetSet_MaxId();
 AssignToPointers();
}


//_SEI_Dis_::_SEI_Dis_(unsigned long long seed, Type Nblocks, Type Degree):
//	MaxTriang(Nblocks*(Degree+1)),
//	Network(Nblocks, Degree),
//	Parameters(NParameters),
//	Results(NResults),
//	EntropyHist(2*(Degree+1UL)*Nblocks+2+1){
// dsfmt_init_gen_rand(&dsfmt, seed);
// AssignToPointers();
//}


void _SEI_Dis_::AssignToPointers(void){
 P2_Param = +Parameters;
 P2_Res = +Results;
 P2_Save = &Save;
}


_SEI_Dis_& _SEI_Dis_::Set_Parameters(void){
	/* Set_InitialConditions();*/
 return *this;
}



bool _SEI_Dis_::Set_InitialConditions(void){
// Parameters[3] /= 2.0;
 T = T_1 = 0;
/* StepSize = exp( (Parameters[8] + fabs((Parameters[12]-2)*Parameters[9]-128.) )*Parameters[10] );*/
/* StepSize *= 1e2;*/
/* StepSize = floor(StepSize);*/
 StepSize = 1e3;
#if _SAVEMOMENTUM_ == 2

/*Set initial conditions*/
/*such that N_IS = z = Parameters[0]*/
/*          N_I = 1*/
/*          N_E = 0*/
 N_IS.realloc( 3*(Parameters[12] - Parameters[9])/Parameters[6]); 
 uint64_t counter = 0;
 for(; counter < N_IS.Get_size()/3; counter++)
	N_IS[counter] = Parameters[12];
 for(; counter < 2*N_IS.Get_size()/3; counter++)
	N_IS[counter] = 1;
 for(; counter < N_IS.Get_size(); counter++)
	N_IS[counter] = 0;

#ifdef _DECAYSIMULATIONS__
 T_Array.Realloc((Parameters[0] - Parameters[9])/Parameters[6]);
 T_Array.Set_to_0();
#endif //_DECAYSIMULATIONS__

#else //_SAVEMOMENTUM_ == 2
/* State.N_IS = Parameters[12];*/
/* State.N_I = 1;*/
 State.ln_IS = log(1.0+1e20);
 State.N_IS = 1e20;
 State.N_IS = State.N_IS < 0.0 ? 0.0 : State.N_IS;
 State.N_I = State.N_IS;

 State.ln_I = log(State.N_I);
 State.P = State.N_IS >=1.0 ? 1.0 : 0.0;
 State_1 = State;
 TimeSeries.counter = 0;
#endif //_SAVEMOMENTUM_ == 2

 Histogram = Parameters[0]/Parameters[1]+1ULL;

#ifndef _DECAYSIMULATIONS__
 int poweroftwo = int( log2(Parameters[4]/Parameters[10])+2ULL);
#else // _DECAYSIMULATIONS__
 double poweroftwo = log2(Parameters[1])+1ULL;//printf("\n\n %lf \n\n", Parameters[1]);
#endif // _DECAYSIMULATIONS__

/* if(fmod(poweroftwo, 1.0) != 0.0){*/
/*	std::cerr << "maximum time is not a power of two!!" << std::endl;*/
/*	exit(123);*/
/* }*/
#ifndef _DECAYSIMULATIONS__
 if(CorrRNGGenerator.size() != pow(2, poweroftwo)){
#else // _DECAYSIMULATIONS__
 if(CorrRNGGenerator.size() != 2ULL*Parameters[1]){
#endif //_DECAYSIMULATIONS__
	CorrRNGGenerator.SetForwardPlan(poweroftwo);
	CorrRNGGenerator.SetCorrelationArray(Parameters[2]);
	gamma_ant = Parameters[2];
	CorrRNGGenerator.SetBackwardPlan();
 }
 else if(fabs(Parameters[2] - gamma_ant) >= 0.00000001){
	CorrRNGGenerator.SetCorrelationArray(Parameters[2]);
	gamma_ant = Parameters[2];
 }
#ifndef _ZEROWIDTH_
 if (DisConf_counter == 0){
	CorrelationArray = CorrRNGGenerator.GetCorrelatedRandomArray(0, Parameters[2]);

	Results = 0.0;
	Results[1] = std::numeric_limits<uint64_t>::max();
	if(TimeSeries.Series.size() < Get_Niter()){
		TimeSeries.Series.resize(Get_Niter());
	}
	for(uint64_t index = 0; index < TimeSeries.Series.size(); index++)
		TimeSeries.Series[index].N_IS = TimeSeries.Series[index].N_I = TimeSeries.Series[index].P = 0;
 }
 DisConf_counter++;
 DisConf_counter = DisConf_counter % int(Parameters[11]);
 if ( DisConf%int(Parameters[11]) != 0){
	printf("Wrong number of disorder configurations per slave\n");
	exit(1);
 }
#endif //_ZEROWIDTH_
 return true;
}

/*Paramters*/
/*[0] - coord number / Max Pos*/
/*[1] -              / Bin Width*/
/*[2] - gamma        / gamma*/
/*[3] - t_init*/
/*[4] - t_end*/
/*[5] - tau*/
/*[6] - none         / beta increase*/
/*[7] -              /bin*/
/*[8] - noise width*/
/*[9] - average beta*/
/*[10]- Delta t*/
/*[11]- NDis per run*/

_SEI_Dis_& _SEI_Dis_::Simulate(void){

#if !defined(_DECAYSIMULATIONS__)
 TimeSeries.counter++;
 while( T < Parameters[3]){
	int u = int( T/ Parameters[10] );
	LocalLambda = CorrelationArray[u];
/*	T += 1./State.N_IS;*/
/*	LocalLambda = Parameters[9] + LocalLambda*Parameters[8];*/
	LocalLambda = LocalLambda*Parameters[8];
	if( State.N_IS > StepSize){
		Results[1] = T;
		LocalLambda += Parameters[9]*(Parameters[12]-2.) - 128.;
		double DeltaT = (int( T/ Parameters[10] )+1.)*Parameters[10];
		DeltaT = DeltaT > Parameters[3] ? Parameters[3] : DeltaT;
		DeltaT -= T;

		T += DeltaT;

		double u = exp(State.ln_IS);
		if (isinf(u)){
			State.ln_IS += LocalLambda*DeltaT/( 128.+Parameters[9] );
		}
		else{
			State.ln_IS = log( 1.0+(u - 1.0)*exp( LocalLambda*DeltaT/( 128.+Parameters[9] ) ) );
		}

		State.N_IS = exp(State.ln_IS);			
		State_1 = State;
		continue;
	}
	else{
		State.N_IS = round(State.N_IS);
		T += -(128.+Parameters[9])*log(dsfmt_genrand_open_close(&dsfmt))/State.N_IS/(128.+LocalLambda+Parameters[9]);
	}
	if( State.N_IS < 1.0){
		State.N_IS = 0.;
		State.P = 0.;
		State.ln_IS = log(1.0);
		State_1 = State;
		T = std::numeric_limits<double>::max();
		break;
	}
#elif _SAVEMOMENTUM_ == 2 && defined(_DECAYSIMULATIONS__)
 for(uint32_t GammaIndex = 0; GammaIndex < State.N_IS.Get_size(); GammaIndex++){
 while( State.N_IS[GammaIndex] < Parameters[3] and T_Array[GammaIndex] < Parameters[1]){
 #ifndef _ZEROWIDTH_
	LocalLambda = CorrelationArray[T_Array[GammaIndex]++];
 #else
	LocalLambda = 0;T_Array[GammaIndex]++;
 #endif //_ZEROWIDTH_

#else //!defined(_DECAYSIMULATIONS__)
 while( exp(-State.N_IS) < Parameters[3] and T < Parameters[1]){
	LocalLambda = CorrelationArray[T++];
#endif // !_DECAYSIMULATIONS__

#if __DISTRIBUTION__ == 2 && _SAVEMOMENTUM_ == 2
 #ifndef _DECAYSIMULATIONS__
	for(uint32_t GammaIndex = 0; GammaIndex <State. N_IS.Get_size()/3; GammaIndex++, LocalLambda += Parameters[6]){
 #else
		LocalLambda += GammaIndex*Parameters[6];
 #endif //_DECAYSIMULATIONS__

	}
#else //__DISTRIBUTION__ == 2 && _SAVEMOMENTUM_ == 2

/*vvvvvvvvvvvvvvsingle simulationvvvvvvvvvvvvvv*/
/*vvvvvvvvvvvvvvsingle simulationvvvvvvvvvvvvvv*/
 State_1 = State;
 if(dsfmt_genrand_close_open(&dsfmt) < Parameters[9]/(128.+Parameters[9]+LocalLambda)){
	State.N_IS += Parameters[12]-2;
	State.N_I++;
 }
 else{
	State.N_IS--;
	State.P = State.N_IS >= 1.0 ? 1.0 : 0.0;
 }
/* State.ln_IS = State.N_IS < 1.0? 0.0 : log(State.N_IS);*/
 State.ln_IS = log(1.0+State.N_IS);
 State.ln_I = log(State.N_I);
/*^^^^^^^^^^^^^^single simulation^^^^^^^^^^^^^^*/
/*^^^^^^^^^^^^^^single simulation^^^^^^^^^^^^^^*/

#endif //__DISTRIBUTION__ == 2 && _SAVEMOMENTUM_ == 2
 }
#ifndef _SAVEMOMENTUM_
 uint64_t bin = State.N_IS + Parameters[1]/2. > Parameters[0] ? Histogram-1 : (State.N_IS + Parameters[1]/2.)/Parameters[1];
 Results[0] = bin;
	bin = 0;
 }
 else{
	double LogPos = log(State.N_IS) + log(Parameters[0]);
	double Width = 2.*log(Parameters[0])/(Histogram-1);
	bin = LogPos + Width/2. < 0.0 ? 0 : (LogPos+Width/2.)/Width;
	bin = bin > Histogram -1ULL ? Histogram -1ULL : bin;
 }
 Results[1] = bin;
#else //_SAVEMOMENTUM_

#ifndef _DECAYSIMULATIONS__

 #if _SAVEMOMENTUM_ == 2
	uint64_t ttemp = 1ULL << 32;

/*	Results[8] = uint64_t(&State.N_IS[0]) & ttemp;*/
/*	Results[9] = (uint64_t(&State.N_IS[0]) >> 32) & ttemp;*/
 #else //_SAVEMOMENTUM_ == 2
	if (DisConf_counter == 0){
		TimeSeries.Series[TimeSeries.counter] += State_1;
		double N_IS_temp = TimeSeries.Series[TimeSeries.counter].N_IS / Parameters[11];
		double N_I_temp  = TimeSeries.Series[TimeSeries.counter].N_I  / Parameters[11];
		double P_temp    = TimeSeries.Series[TimeSeries.counter].P    / Parameters[11];
/*		double lnis_Temp = TimeSeries.Series[TimeSeries.counter].ln_IS/Parameters[11];*/

		Results[0] = 1.0;
		Results[2] = N_IS_temp;
		Results[3] = N_IS_temp*N_IS_temp;
		Results[4] = 0*N_I_temp;
		Results[5] = 0*N_I_temp*N_I_temp;
/*		double x = N_IS_temp > 0.0 ? log(N_IS_temp) : 0.0;*/
/*		x += log(Parameters[11])+1.;*/
		double x = log(1.0 + N_IS_temp);
		Results[6] = x;
		Results[7] = x*x;
		Results[8] = P_temp;
		Results[9] = P_temp*P_temp;
	}
	else{
		TimeSeries.Series[TimeSeries.counter] += State_1;
	}
 #endif //_SAVEMOMENTUM_ == 2

#else // _DECAYSIMULATIONS__

 #if _SAVEMOMENTUM_ == 2
	uint64_t ttemp = 1ULL << 32;
	ttemp--;
	Results[8] = uint64_t(&T_Array[0]) & ttemp;
	Results[9] = (uint64_t(&T_Array[0]) >> 32) & ttemp;
 #else //_SAVEMOMENTUM_ == 2
	if (DisConf_counter = DisConf - 1){
		TimeSeries[DisConf_Counter];
	}
	else{
		Results[0] = 1.;
		Results[1] = std::numeric_limits<uint64_t>::max();
		Results[2] = T;
		Results[3] = T*T;
		Results[4] = Results[3]*T;
		Results[5] = Results[3]*Results[3];
		double rho = exp(1.*State.N_IS);
/*		Results[6] = rho;*/
		Results[7] = rho*rho;
		Results[8] = Results[7]*rho;
		Results[9] = Results[7]*Results[7];
	}
 #endif //_SAVEMOMENTUM_ == 2

#endif //_DECAYSIMULATIONS__

#endif //_SAVEMOMENTUM_

 return *this;
}

_SEI_Dis_& _SEI_Dis_::Save_Simulation(long long Id){
#ifndef _SAVEMOMENTUM_
 if (Results[0] == 0.0 and Results[1] == 0.0){
	return *this;
 }
 Save.Exec("Update "+Save.Get_TableName()+" SET count = count+"+std::to_string(Results[0])+", count_ln = count_ln + "+std::to_string(Results[1])+" WHERE Id ="+std::to_string(Id)+";");
#else
 _MPI_vector_<double> Temp(NResults);
 Save.SearchforId( Id, Temp.Get_Pointer());
 if(Temp[1] != 0.)
	Results[1] = Results[1] < Temp[1] ? Results[1] : Temp[1];

 std::stringstream temp;
 temp << std::scientific << std::setprecision(14) << Results[0];
 std::string Command = "Update "+Save.Get_TableName()+" SET count=count+"+temp.str()+", ";

 temp.str(std::string());
 temp << std::scientific << std::setprecision(14) << Results[1];
 Command += "MinTime = "+temp.str()+", ";

 temp.str(std::string());
 temp << std::scientific << std::setprecision(14) << Results[2];
 Command += "N_IS = N_IS + "+temp.str()+", ";

 if(isinf(Results[3])){
	Command += "N_IS2 = NULL, ";
 }
 else{
	temp.str(std::string());
	temp << std::scientific << std::setprecision(14) << Results[3];
	Command += "N_IS2 = N_IS2 + "+temp.str()+", ";
 }

 if(isinf(Results[4])){
	Command += "N_I = NULL, ";
 }
 else{
	temp.str(std::string());
	temp << std::scientific << std::setprecision(14) << Results[4];
	Command += "N_I = N_I + "+temp.str()+", ";
 }

 if(isinf(Results[5])){
	Command += "N_I = NULL, ";
 }
 else{
	temp.str(std::string());
	temp << std::scientific << std::setprecision(14) << Results[5];
	Command += "N_I2 = N_I2 + "+temp.str()+", ";
 }

 temp.str(std::string());
 temp << std::scientific << std::setprecision(14) << Results[6];
/* std::cout << temp.str() << std::endl;*/
 Command += "x = x + "+temp.str()+", ";

 temp.str(std::string());
 temp << std::scientific << std::setprecision(14) << Results[7];
 Command += "x2 = x2 + "+temp.str()+", ";

 temp.str(std::string());
 temp << std::scientific << std::setprecision(14) << Results[8];
 Command += "rho3 = rho3 + "+temp.str()+", ";

 temp.str(std::string());
 temp << std::scientific << std::setprecision(15) << Results[9];
 Command += "rho4 = rho4 + "+temp.str()+" ";

 Command += "WHERE Id ="+std::to_string(Id)+";";

/* std::string Command = "Update "+Save.Get_TableName()+" SET count=count+"+std::to_string(Results[0])+", ";*/
/* Command += "MinTime = "+std::to_string(Results[1])+", ";*/
/* Command += "N_IS = N_IS + "+std::to_string(Results[2])+", ";*/
/* Command += "N_IS2 = N_IS2 + "+std::to_string(Results[3])+", ";*/
/* Command += "N_I = N_I + "+std::to_string(Results[4])+", ";*/
/* Command += "N_I2 = N_I2 + "+std::to_string(Results[5])+", ";*/
/* Command += "x = x + "+std::to_string(Results[6])+", ";*/
/* Command += "x2 = x2 + "+std::to_string(Results[7])+", ";*/
/* Command += "rho3 = rho3 + "+std::to_string(Results[8])+", ";*/
/* Command += "rho4 = rho4 + "+std::to_string(Results[9])+" ";*/
/* Command += "WHERE Id ="+std::to_string(Id)+";";*/

 Save.Exec(Command);
#endif// _SAVEMOMENTUM_
 return *this;
}


uint64_t _SEI_Dis_::Get_Niter(void){
 #ifdef _Niter_Linear_
	return Parameters[4]/Parameters[5]+1;
 #elif defined(_Niter_Multiply_)
	uint64_t temp = uint64_t( log(Parameters[4])/log(Parameters[5])+1 );
	return temp;
 #endif /* _Niter_Linear_*/
}


long long _SEI_Dis_::Init_Database(void){
 return Save.AssignId(Parameters.Get_Pointer());
}

void _SEI_Dis_::BeginTransaction(void){
 Save.Exec("BEGIN TRANSACTION");
}

void _SEI_Dis_::EndTransaction(void){
 Save.Exec("END TRANSACTION");
}
/***********************************************
l
   RELATED TO _Parallelize_CondTimeSeries_.h

***********************************************/

void _Parallelize_CondTimeSeries_::Save_TimeSeries(void){
 time_t t1, t2;
 time(&t1);
 Par_Simul->BeginTransaction();
 unsigned long long MaxBins = TMP_Serie.Get_Received_size();
// (*Par_Simul->P2_Param) = TMP_Serie.Parameters;
 for(unsigned long long bin = 0; bin < MaxBins; bin++){
//	(*Par_Simul->P2_Param)[7] = bin;
	TMP_Serie.Get_line(*Par_Simul->P2_Res, bin);
	Par_Simul->Save_Simulation(TMP_Serie.Id[bin]);//if ((*Par_Simul->P2_Res)[1] !=0) printf("%lf\n", (*Par_Simul->P2_Res)[1]);
 }
 Par_Simul->EndTransaction();
 time(&t2);
 printf("Time to save %lf\n", difftime(t2, t1));
}

unsigned long long _Parallelize_CondTimeSeries_::Init_Database(_Time_Series_ &TMP){
 time_t t1, t2;
 time(&t1);
 uint64_t Niter = Get_Niter(TMP.Parameters[4], TMP.Parameters[3], TMP.Parameters[5]);
// Niter++;
#ifndef _SAVEMOMENTUM_
 unsigned long long MaxBins = TMP.Parameters[0]/TMP.Parameters[1] + 1.0;
#elif _SAVEMOMENTUM_ == 2
 unsigned long long MaxBins = (TMP.Parameters[0] - TMP.Parameters[9])/TMP.Parameters[6];
 double temp0 = TMP.Parameters[0];
 double temp6 = TMP.Parameters[6];
 double temp9 = TMP.Parameters[9];
 TMP.Parameters[0] = 0.;
 TMP.Parameters[6] = 0.;
 TMP.Parameters[9] = 0.;
#else // _SAVEMOMENTUM_
 unsigned long long MaxBins = 1;
#endif //_SAVEMOMENTUM_
 if( MaxBins*Niter > TMP.Get_Res_ArraySize()) TMP.Realloc_TimeSeries(MaxBins*Niter);
 double temp = TMP.Parameters[3];
 double temp4 = TMP.Parameters[4];
 double temp5 = TMP.Parameters[5];
 TMP.Parameters[4] = 0.;
#ifdef _ZEROPARAM5_
 TMP.Parameters[5] = 0.;
#endif
 Par_Simul->BeginTransaction();
 uint64_t MaxId = Par_Simul->P2_Save->GetSet_MaxId();
 for(uint64_t iter = 0; iter < Niter; AssignTime(*TMP.Parameters, iter, temp5)){
	(*Par_Simul->P2_Param) = TMP.Parameters;
	unsigned long long bin = 0;
	(*Par_Simul->P2_Param)[7] = bin;
	#if _SAVEMOMENTUM_ != 2
	uint32_t NumberofMatches = Par_Simul->P2_Save->Searchfor(Par_Simul->P2_Param->Get_Pointer());
	if(NumberofMatches == 0){
		for(; bin < MaxBins; bin++){
			(*Par_Simul->P2_Param)[7] = bin;
			TMP.Id[bin+iter*MaxBins] = ++MaxId;
			Par_Simul->P2_Save->InsertId(Par_Simul->P2_Param->Get_Pointer(), MaxId);
		}
	}
	else if(NumberofMatches == 1){
		TMP.Id[iter*MaxBins] = Par_Simul->Init_Database();
		uint64_t TempId = TMP.Id[iter*MaxBins];
		bin++;
		for(; bin < MaxBins; bin++){
			TMP.Id[bin+iter*MaxBins] = TempId+bin;
		}
	}
	else{
		std::cout << "Wrong number of matches, please check database insertion" << std::endl;
		exit(12);
	}
	#elif _SAVEMOMENTUM_ == 2
	for(; bin < MaxBins; bin++){
		(*Par_Simul->P2_Param)[9] = temp9 + bin*temp6;
		uint32_t NumberofMatches = Par_Simul->P2_Save->Searchfor(Par_Simul->P2_Param->Get_Pointer());
		if(NumberofMatches == 0){
			TMP.Id[bin+iter*MaxBins] = ++MaxId;
			Par_Simul->P2_Save->InsertId(Par_Simul->P2_Param->Get_Pointer(), MaxId);			
		}
		else if(NumberofMatches == 1){
			TMP.Id[iter*MaxBins+bin] = Par_Simul->Init_Database();
/*			uint64_t TempId = TMP.Id[iter*MaxBins];*/
/*			TMP.Id[bin+iter*MaxBins] = TempId+bin;*/
		}
		else{
			std::cout << "Wrong number of matches, please check database insertion" << std::endl;
			exit(12);
		}
	}
	#endif //_SAVEMOMENTUM_ == 2
	iter++;
 }
 Par_Simul->EndTransaction();
 Par_Simul->P2_Save->GetSet_MaxId();
 TMP.Parameters[3] = temp;
 TMP.Parameters[4] = temp4;
 TMP.Parameters[5] = temp5;
#if _SAVEMOMENTUM_ == 2
 TMP.Parameters[0] = temp0;
 TMP.Parameters[6] = temp6;
 TMP.Parameters[9] = temp9;
#endif //_SAVEMOMENTUM_ == 2
 time(&t2);
 printf("Database Init time %lf\n", difftime(t2, t1));
 return MaxBins*Niter;
}

void _Parallelize_CondTimeSeries_::no_means_func(_MPI_vector_<double> &Results, unsigned long long x){
#ifndef _SAVEMOMENTUM_
 uint64_t histsize = Get_Niter(TMP_Serie.Parameters[4], TMP_Serie.Parameters[3], TMP_Serie.Parameters[5]);
 histsize = TMP_Serie.Get_Received_size()/histsize;
 TMP_Serie.Res_Array[Results.Get_size()*(Results[0]+histsize*x)] += 1.0;
 TMP_Serie.Res_Array[Results.Get_size()*(Results[1]+histsize*x)+1] += 1.0;

#elif _SAVEMOMENTUM_==2
 uint64_t histsize = (TMP_Serie.Parameters[0] - TMP_Serie.Parameters[9])/TMP_Serie.Parameters[6];
#if defined(_DECAYSIMULATIONS__)
 uint64_t *Res_Array = (uint64_t*) ( uint64_t(Results[8]) + (uint64_t(Results[9]) << 32) );
#else //defined(_DECAYSIMULATIONS__)
 double *Res_Array = (double*) ( uint64_t(Results[8]) + (uint64_t(Results[9]) << 32) );
#endif //defined(_DECAYSIMULATIONS__)
 for(uint32_t index = 0; index < histsize; index++){
	uint64_t offset = Results.Get_size()*histsize*x + Results.Get_size()*index;
/*	printf("offset %lu %lu\n", offset, TMP_Serie.Get_Received_size());*/
	TMP_Serie.Res_Array[offset+0] += 1.0;
	TMP_Serie.Res_Array[offset+1]  = 0.;
	TMP_Serie.Res_Array[offset+2] += Res_Array[index];
	TMP_Serie.Res_Array[offset+3] += Res_Array[index]*Res_Array[index];
	TMP_Serie.Res_Array[offset+4] += pow(Res_Array[index], 3);
	TMP_Serie.Res_Array[offset+5] += pow(Res_Array[index], 4);
#if defined(_DECAYSIMULATIONS__)
	double rho = exp(-double(Res_Array[index]));
	TMP_Serie.Res_Array[offset+6] += rho;
	TMP_Serie.Res_Array[offset+7] += rho*rho;
	TMP_Serie.Res_Array[offset+8] += pow(rho, 3);
	TMP_Serie.Res_Array[offset+9] += pow(rho, 4);
#else //defined(_DECAYSIMULATIONS__)
	double rho = exp(-Res_Array[index]);
	TMP_Serie.Res_Array[offset+6] += rho;
	TMP_Serie.Res_Array[offset+7] += rho*rho;
	TMP_Serie.Res_Array[offset+8] += pow(rho, 3);
	TMP_Serie.Res_Array[offset+9] += pow(rho, 4);
#endif //defined(_DECAYSIMULATIONS__)
 }

#else //_SAVEMOMENTUM_
 TMP_Serie.Res_Array[Results.Get_size()*x] += Results[0];
 if(TMP_Serie.Res_Array[Results.Get_size()*x+1] == 0.){
	TMP_Serie.Res_Array[Results.Get_size()*x+1] = Results[1];
 }
 else{
	TMP_Serie.Res_Array[Results.Get_size()*x+1] = TMP_Serie.Res_Array[Results.Get_size()*x+1] > Results[1] ? Results[1] : TMP_Serie.Res_Array[Results.Get_size()*x+1];
 }
 TMP_Serie.Res_Array[Results.Get_size()*x+2] += Results[2];
 TMP_Serie.Res_Array[Results.Get_size()*x+3] += Results[3];
 TMP_Serie.Res_Array[Results.Get_size()*x+4] += Results[4];
 TMP_Serie.Res_Array[Results.Get_size()*x+5] += Results[5];
 TMP_Serie.Res_Array[Results.Get_size()*x+6] += Results[6];
 TMP_Serie.Res_Array[Results.Get_size()*x+7] += Results[7];
 TMP_Serie.Res_Array[Results.Get_size()*x+8] += Results[8];
 TMP_Serie.Res_Array[Results.Get_size()*x+9] += Results[9];
#endif // _SAVEMOMENTUM_
}

void _Parallelize_CondTimeSeries_::no_means_BEFORESEND_func(_MPI_vector_<double> &Results){
 unsigned long long MaxBins = TMP_Serie.Get_Received_size();
// TMP_Serie.Parameters[4] = 0.0;
// TMP_Serie.Parameters[5] = 0.0;

// for(unsigned long long bin = 0; bin < MaxBins; bin++){
//	TMP_Serie.Res_Array[Results[0]] += 1.0;
//	TMP_Serie.Res_Array[bin*Results[Results.Get_size()-1]] = 1.0;
// }
 TMP_Serie.Send_Partial_TimeSeries(0, 0, MaxBins);
}
#endif


