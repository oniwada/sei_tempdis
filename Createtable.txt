CREATE TABLE FracBrowMotion(
	Max_Pos REAL,
	BWidth REAL,
	gamma REAL,
	t_init BIGINT,
	t_max BIGINT,
	tau REAL,
	mu REAL,
	bin BIGINT,
	C REAL,
	p REAL,
	DeltaT REAL,
	count BIGINT,
	count_ln BIGINT,
	Id BIGINT UNIQUE,
	PRIMARY KEY(Max_Pos, BWidth, gamma, t_init, t_max, tau, mu, bin, C, p, DeltaT))

Max_Pos = coordNumber

CREATE TABLE Momentus(
	Max_Pos REAL,
	BWidth REAL,
	gamma REAL,
	t_init BIGINT,
	t_max BIGINT,
	tau REAL,
	mu REAL,
	bin BIGINT,
	sigma REAL,
	beta REAL,
	DeltaT REAL,
	DisPerRun BIGINT,
	CoordN INT,
	count BIGINT,
	MinTime BIGINT,
	N_IS REAL,
	N_IS2 REAL,
	N_I REAL,
	N_I2 REAL,
	x REAL,
	x2 REAL,
	rho3 REAL,
	rho4 REAL,
	Id BIGINT UNIQUE,
	PRIMARY KEY(Max_Pos, BWidth, gamma, t_init, t_max, tau, mu, bin, sigma, beta, DeltaT, DisPerRun, CoordN))

CREATE TABLE Criticality(
	gamma REAL,
	t BIGINT,
	t_init BIGINT,
	t_max BIGINT,
	Drift REAL,
	Exp REAL,
	SExp REAL,
	ExpT REAL,
	SExpT REAL,
	CoefB REAL,
	SCoefB REAL,
	PRIMARY KEY(gamma,t , t_init, t_max, drift))

CREATE TABLE tau(
	gamma REAL,
	t BIGINT,
	t_init BIGINT,
	t_max BIGINT,
	Drift REAL,
	Exp REAL,
	SExp REAL,
	CoefB REAL,
	SCoefB REAL,
	PRIMARY KEY(gamma,t , t_init, t_max, drift))

INSERT INTO FracBrowMotion Values(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
